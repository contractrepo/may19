package com.may19.may19.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ListeningImageView extends ImageView {

	public ListeningImageView(Context aContext, AttributeSet aAttrs,
			int aDefStyleAttr) {
		super(aContext, aAttrs, aDefStyleAttr);
	}

	public ListeningImageView(Context aContext, AttributeSet aAttrs) {
		super(aContext, aAttrs);
	}

	public ListeningImageView(Context aContext) {
		super(aContext);
	}

	private OnImageChangeListener iOnImageChangeListener;

	public void setImageChangeListener(
			OnImageChangeListener aOnImageChangeListener) {
		iOnImageChangeListener=aOnImageChangeListener;
	}

	@Override
	public void setImageBitmap(Bitmap aBitmap) {
		super.setImageBitmap(aBitmap);
		if(iOnImageChangeListener!=null)
			iOnImageChangeListener.bitmapChangedinView((Integer) getTag(),
					aBitmap);
	}

	public static interface OnImageChangeListener {
		public void bitmapChangedinView(int aPosition, Bitmap aBitmap);
	}
}
