package com.may19.may19.content;


public class ImageData {

	private String iUrl;
	private String iSourceUrl;
	private String iId;

	public void setUrl(final String aUrl) {
		iUrl=aUrl;
	}

	public void setSourceUrl(final String aSourceUrl) {
		iSourceUrl=aSourceUrl;
	}

	public void setId(final String aId) {
		iId=aId;
	}
	public String getId() {
		return iId;
	}
	public String getUrl() {
		return iUrl;
	}
	public String getSourceUrl() {
		return iSourceUrl;
	}

	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append(" URL=").append(iUrl).append(" SOURCE=").append(iSourceUrl)
				.append(" ID=").append(iId);
		return sb.toString();
	}
}
