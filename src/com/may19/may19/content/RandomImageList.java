package com.may19.may19.content;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class RandomImageList {

	private static final String XML_URL="url";
	private static final String XML_SOURCE_URL="source_url";
	private static final String XML_ID="id";
	private static final String XML_IMAGE="image";
	private ArrayList<ImageData> iImageList=new ArrayList<ImageData>();
	private ImageData iCurrentImage;

	public RandomImageList(String aXmlStr) throws XmlPullParserException,
			IOException {
		XmlPullParserFactory factory=XmlPullParserFactory.newInstance();
		XmlPullParser xpp=factory.newPullParser();
		xpp.setInput(new StringReader(aXmlStr));
		int eventType=xpp.getEventType();
		while(eventType!=XmlPullParser.END_DOCUMENT) {
			switch(eventType) {
				case XmlPullParser.START_TAG: {
					String tagName=xpp.getName();
					if(tagName.equalsIgnoreCase(XML_IMAGE)) {
						iCurrentImage=new ImageData();
						break;
					}
					if(tagName.equals(XML_URL)) {
						iCurrentImage.setUrl(xpp.nextText());
						break;
					}
					if(tagName.equals(XML_SOURCE_URL)) {
						iCurrentImage.setSourceUrl(xpp.nextText());
						break;
					}
					if(tagName.equals(XML_ID)) {
						iCurrentImage.setId(xpp.nextText());
						break;
					}
					break;
				}
				case XmlPullParser.END_TAG: {
					String tagName=xpp.getName();
					if(tagName.equalsIgnoreCase(XML_IMAGE)) {
						iImageList.add(iCurrentImage);
						break;
					}
					break;
				}
				default:
					break;
			}
			eventType=xpp.next();
		}
	}

	public ArrayList<String> getImageUrls() {
		ArrayList<String> result=new ArrayList<String>();
		for(ImageData id: iImageList)
			result.add(id.getUrl());
		return result;
	}

	public String getSourceUrl(int aPos) {
		if(aPos<0||aPos>iImageList.size()-1) return null;
		return iImageList.get(aPos).getSourceUrl();
	}

	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder();
		for(ImageData id: iImageList)
			sb.append(id).append(", ");
		return sb.toString();
	}

}
