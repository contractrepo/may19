package com.may19.may19.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.may19.may19.R;

public class Splash extends Activity {

	private static int SPLASH_TIME_OUT=5000;

	@Override
	protected void onCreate(Bundle aBundle) {
		super.onCreate(aBundle);
		setContentView(R.layout.splash);
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				Intent i=new Intent(Splash.this, Main.class);
				startActivity(i);
				finish();
			}
		}, SPLASH_TIME_OUT);
	}
}
