package com.may19.may19.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;

import com.may19.may19.Constants;
import com.may19.may19.R;
import com.may19.may19.adapters.GalleryAdapter;
import com.may19.may19.adapters.GalleryAdapter.BitmapChangedListener;
import com.may19.may19.content.RandomImageList;
import com.may19.may19.http.ApiService;
import com.may19.may19.http.ApiService.ApiServiceListener;

@SuppressWarnings("deprecation")
public class Main extends Activity
		implements
			ApiServiceListener,
			OnItemSelectedListener,
			BitmapChangedListener,
			OnClickListener {

	private String iGetUuid;
	private GalleryAdapter iAdapter;
	private ImageView iImage;
	private int iCurrentImageIndex;
	private TextView iSource;
	private RandomImageList iCurrentList;
	private Button iRefresh;

	@Override
	protected void onCreate(Bundle aBundle) {
		super.onCreate(aBundle);
		setContentView(R.layout.main);
		handleViews();
		getRandomList();
	}

	private void handleViews() {
		iSource=(TextView) findViewById(R.id.source);
		iImage=(ImageView) findViewById(R.id.image);
		iRefresh=(Button) findViewById(R.id.refresh);
		iRefresh.setOnClickListener(this);
		Gallery g=(Gallery) findViewById(R.id.gallery);
		g.setOnItemSelectedListener(this);
		iAdapter=new GalleryAdapter(this, this);
		g.setAdapter(iAdapter);

	}

	private void getRandomList() {
		iGetUuid=ApiService.getRandomList(this, Constants.NUMBER_OF_IMAGES);
	}

	public void populate() {
		iAdapter.clear();
		iAdapter.setData(iCurrentList.getImageUrls());
		iAdapter.notifyDataSetChanged();
	}

	@Override
	public void notifyFailure(String aUuid, int aErrorCode, String aErrorMessage) {
		if(iGetUuid.equals(aUuid)) {
			System.out
					.println("RICK: GET FAILED "+aErrorCode+" "+aErrorMessage);
			// TODO: HANDLE THIS
		}
	}

	@Override
	public void notifySuccess(String aUuid, Object aResult) {
		iCurrentList=(RandomImageList) aResult;
		if(iGetUuid.equals(aUuid)) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					populate();
				}
			});
			return;
		}
	}
	@Override
	public void onItemSelected(AdapterView<?> aAdapterView, View aView,
			int aPos, long aId) {
		iCurrentImageIndex=aPos;
		Bitmap bm=(Bitmap) iAdapter.getItem(aPos);
		iImage.setImageBitmap(bm);
		iSource.setText(iCurrentList.getSourceUrl(aPos));
	}

	@Override
	public void onNothingSelected(AdapterView<?> aAdapterView) {}

	@Override
	public void notifyBitmapChanged(int aPos, Bitmap aBitmap) {
		if(aPos!=iCurrentImageIndex) return;
		iImage.setImageBitmap(aBitmap);
	}

	@Override
	public void onClick(View aView) {
		if(aView==iRefresh) {
			getRandomList();
			return;
		}
	}
}
