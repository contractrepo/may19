package com.may19.may19.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Utils {

	public static void copyStream(InputStream aIs, OutputStream aOs) {
		final int buffer_size=1024;
		try {
			byte[] bytes=new byte[buffer_size];
			for(;;) {
				int count=aIs.read(bytes, 0, buffer_size);
				if(count==-1) break;
				aOs.write(bytes, 0, count);
			}
		}
		catch(Exception ex) {}
	}

	public static byte[] toByteArray(InputStream aInputStream)
			throws IOException {
		ByteArrayOutputStream buffer=new ByteArrayOutputStream();
		int nRead;
		byte[] data=new byte[16384];
		while((nRead=aInputStream.read(data, 0, data.length))!=-1) {
			buffer.write(data, 0, nRead);
		}
		buffer.flush();
		return buffer.toByteArray();
	}
}
