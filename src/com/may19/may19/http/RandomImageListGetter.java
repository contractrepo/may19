package com.may19.may19.http;

import java.io.UnsupportedEncodingException;

import android.content.ContentValues;

public class RandomImageListGetter extends HttpGetter {

	public RandomImageListGetter(final String aUuid, ContentValues aParams,
			HttpGetterListener aListener) throws UnsupportedEncodingException {
		super(aListener, aUuid, "images/get", aParams);
	}
}
