package com.may19.may19.http;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

import android.content.ContentValues;

import com.may19.may19.Constants;

public abstract class HttpGetter extends Thread {

	public interface HttpGetterListener {

		public void notifySuccess(final String aUuid, final String aResult);

		public void notifyFailure(final String aUuid, int aErrorCode,
				final String aErrorMessage);
	}

	protected String iUuid;
	private HttpGetterListener iListener;
	private URL iUrl;

	protected HttpGetter(final HttpGetterListener aListener,
			final String aUuid, final String aEndPoint,
			final ContentValues aParameters) {
		iListener=aListener;
		iUuid=aUuid;
		String urlStr=Constants.API_END_POINT+aEndPoint;
		if(aParameters!=null&&aParameters.size()>0) {
			urlStr+="?";
			Iterator<String> iter=aParameters.keySet().iterator();
			while(iter.hasNext()) {
				String key=iter.next();
				String value=aParameters.getAsString(key);
				urlStr+=key+"="+value;
				if(iter.hasNext()) urlStr+="&";
			}
		}
		try {
			iUrl=new URL(urlStr);
		}
		catch(MalformedURLException ex) {
			notifyFailure(-1, "REQUEST FAILED: "+ex.toString());
			ex.printStackTrace();
		}
	}

	@Override
	public void run() {
		BufferedReader br=null;
		InputStream is=null;
		try {
			HttpURLConnection conn=(HttpURLConnection) iUrl.openConnection();
			conn.setDoInput(true);
			conn.setRequestMethod("GET");
			conn.connect();
			is=new BufferedInputStream(conn.getInputStream());
			br=new BufferedReader(new InputStreamReader(is));
			StringBuilder sb=new StringBuilder();
			String line=null;
			while((line=br.readLine())!=null) {
				sb.append(line);
			}
			notifySuccess(sb.toString());
		}
		catch(Exception ex) {
			ex.printStackTrace();
			System.out.println("RICK: HTTPGETTER: ERROR GETTING RESPONSE: "
					+ex.toString());
			notifyFailure(-1, ex.toString());
		}
		finally {
			try {
				if(is!=null) is.close();
			}
			catch(Exception ex) {}
		}
	}

	private void notifyFailure(int aErrorCode, final String aErrorMessage) {
		if(iListener!=null)
			iListener.notifyFailure(iUuid, aErrorCode, aErrorMessage);
	}

	private void notifySuccess(final String aResult) {
		if(iListener!=null) iListener.notifySuccess(iUuid, aResult);
	}

}
