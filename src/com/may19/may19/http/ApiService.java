package com.may19.may19.http;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.UUID;

import android.content.ContentValues;

import com.may19.may19.content.RandomImageList;
import com.may19.may19.http.ApiService.HttpGetterListenerImpl.EGetApi;
import com.may19.may19.http.HttpGetter.HttpGetterListener;

public class ApiService {

	public interface ApiServiceListener {

		public void notifySuccess(String aUuid, Object aResult);

		public void notifyFailure(String aUuid, int aErrorCode,
				String aErrorMessage);
	}

	private static HttpGetterListenerImpl iGetterListener=new HttpGetterListenerImpl();
	private static HashMap<String, ApiServiceListener> iListeners=new HashMap<String, ApiServiceListener>();

	private ApiService() {}

	public static class HttpGetterListenerImpl implements HttpGetterListener {

		public enum EGetApi {
			ERandomImageList;
		}

		private HashMap<String, EGetApi> iUuidMap=new HashMap<String, EGetApi>();

		@Override
		public void notifySuccess(String aUuid, String aResult) {
			if(iUuidMap.containsKey(aUuid)) {
				System.out.println("RICK: GET API SERVICE SUCCESS "
						+iUuidMap.get(aUuid));
				switch(iUuidMap.get(aUuid)) {
					case ERandomImageList:
						if(iListeners.containsKey(aUuid)) {
							try {
								iListeners.get(aUuid).notifySuccess(aUuid,
										new RandomImageList(aResult));
							}
							catch(Exception ex) {
								iListeners.get(aUuid).notifyFailure(aUuid, -1,
										ex.toString());
							}
							iListeners.remove(aUuid);
						}
						break;
					default:
						break;
				}
				iUuidMap.remove(aUuid);
			}
		}

		@Override
		public void notifyFailure(String aUuid, int aErrorCode,
				String aErrorMessage) {
			System.out.println("RICK: GET API SERVICE FAILURE "+aErrorMessage);
			if(iUuidMap.containsKey(aUuid)) {
				iListeners.get(aUuid).notifyFailure(aUuid, aErrorCode,
						aErrorMessage);
				iListeners.remove(aUuid);
				iUuidMap.remove(aUuid);
			}
		}

		public void addListener(EGetApi aApi, final String aUuid,
				final ApiServiceListener aListener) {
			iUuidMap.put(aUuid, aApi);
			iListeners.put(aUuid, aListener);
		}
	}

	public static String getRandomList(ApiServiceListener aListener, int aCount) {
		try {
			String uuid=UUID.randomUUID().toString();
			iGetterListener.addListener(EGetApi.ERandomImageList, uuid,
					aListener);
			ContentValues params=new ContentValues();
			params.put("format", "xml");
			params.put("results_per_page", String.valueOf(aCount));
			RandomImageListGetter rg=new RandomImageListGetter(uuid, params,
					iGetterListener);
			rg.start();
			return uuid;
		}
		catch(UnsupportedEncodingException ex) {
			return null;
		}
	}
}
