package com.may19.may19;

public class Constants {
	public static final String CAT_API="MTY0MDA";
	public static final String API_END_POINT="http://thecatapi.com/api/";
	public static final int NUMBER_OF_IMAGES=10;
	public static final String CACHE_DIR="may19/cache";
}
