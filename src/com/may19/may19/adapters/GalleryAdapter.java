package com.may19.may19.adapters;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.may19.may19.R;
import com.may19.may19.utils.ImageLoader;
import com.may19.may19.views.ListeningImageView;
import com.may19.may19.views.ListeningImageView.OnImageChangeListener;

public class GalleryAdapter extends BaseAdapter
		implements
			OnImageChangeListener {

	private class ItemData {
		private String iUrl;
		private ListeningImageView iImageView;
	}

	private ArrayList<ItemData> iData=new ArrayList<ItemData>();
	private ImageLoader iImageLoader;
	private LayoutInflater iInflater;
	private BitmapChangedListener iListener;

	public GalleryAdapter(Activity aActivity, BitmapChangedListener aListener) {
		iListener=aListener;
		iInflater=(LayoutInflater) aActivity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		iImageLoader=new ImageLoader(aActivity.getApplicationContext());
	}

	public int getCount() {
		return iData==null?0:iData.size();
	}

	public Object getItem(int aPos) {
		iData.get(aPos).iImageView.buildDrawingCache();
		return iData.get(aPos).iImageView.getDrawingCache();
	}

	public long getItemId(int aPos) {
		return aPos;
	}

	public void clear() {
		if(iData!=null) iData.clear();
	}

	public void setData(ArrayList<String> aData) {
		for(String d: aData) {
			ItemData id=new ItemData();
			id.iUrl=d;
			iData.add(id);
		}
	}

	private class ViewHolder {
		public ListeningImageView iImageView;
	}

	@SuppressLint("InflateParams")
	public View getView(int aPos, View aView, ViewGroup aParent) {
		View view=aView;
		ViewHolder holder=null;
		if(view==null) {
			view=iInflater.inflate(R.layout.gallery_item, null);
			holder=new ViewHolder();
			holder.iImageView=(ListeningImageView) view
					.findViewById(R.id.gallery_image);
			iData.get(aPos).iImageView=holder.iImageView;
			iData.get(aPos).iImageView.setTag(aPos);
			iData.get(aPos).iImageView.setImageChangeListener(this);
			view.setTag(holder);
		} else {
			holder=(ViewHolder) view.getTag();
		}
		iImageLoader.displayImage(iData.get(aPos).iUrl, holder.iImageView);
		return view;
	}

	@Override
	public void bitmapChangedinView(int aPos, Bitmap aBitmap) {
		System.out.println("RICK: IMAGE CHANGED AT POSITION "+aPos);
		if(iListener!=null) iListener.notifyBitmapChanged(aPos, aBitmap);
	}
	public static interface BitmapChangedListener {
		public void notifyBitmapChanged(int aPos, Bitmap aBitmap);
	}

}
